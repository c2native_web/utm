import { copyToClipBoard } from "./helper.js";
//catch html result area
const resultArea = document.querySelector("#result-area");
const resultText = document.querySelector("#reslut");
const resultRadyToCopyText = document.querySelector("#resluts-tocopy");
export function buildUrl(fomrData) {
	const urlOptions = {
		url: removeLastSlash(fomrData.get('url')),
		source: fomrData.get('source'),
		medium: fomrData.get('medium'),
		content: fomrData.getAll('content'),
		term: fomrData.getAll('term'),
		language: fomrData.get('language'),
		campaign: fomrData.get('campaign'),
		audiance: fomrData.get('audiance')
	}
	//show result area
	resultArea.classList.replace('d-none', 'd-flex');

	//genrate url
	const urlsArray = urlGenerator(urlOptions);

	//print url result to screen
	resultText.innerHTML = printUrl(urlsArray);
	
	//ready to copy url result to screen
	resultRadyToCopyText.innerHTML = printReadyToCopyUrl(urlsArray);

	//register event listener for copy btn
	copyBtnEventlistenr();

	//scroll screen to view results
	resultArea.scrollIntoView();
}

function urlGenerator(urlOptions) {
	let printableUrls = [];
	//create urls for each content
	urlOptions.content.forEach(content => {
		//append current url
		let url = urlOptions.url;
		//set the source
		url += "/?utm_source=" + urlOptions.source;
		//set medium
		url += "&utm_medium=" + urlOptions.medium;
		//set content
		url += "&utm_content=" + content;
		//set language
		urlOptions.language ? url += "&utm_language=" + urlOptions.language : '';
		//set campaign
		urlOptions.campaign ? url += "&utm_campaign=" + urlOptions.campaign : '';
		//set audiance
		urlOptions.audiance && urlOptions.campaign ? url += "&utm_campaign_audiance=" + urlOptions.audiance : '';
		//set term
		if (urlOptions.term.length) {
			urlOptions.term.forEach(term => {
				url += "&utm_term=" + term;
				printableUrls.push({ url: url, content: content.replace(/%20/g, " ") + " with " + term.replace(/%20/g, " ") });
			});
		}
		else {
			printableUrls.push({ url: url, content: content.replace(/%20/g, " ") });
		}
	});

	return printableUrls;
}

function printUrl(urlsArray) {
	//prepire printable html elements
	let htmlView = "<ul class='list-unstyled'>";
	//loop throw urls to print reguler results
	urlsArray.forEach(url => {
		htmlView += "<li>";
		htmlView += `<h6>${url.content} : <a href="javascript:void(0)" class="float-right text-muted copy-btn" data-toggle="tooltip" title="Copied !"><i class="fa fa-copy"></i></a></h6>`
		htmlView += `<textarea class='form-control'>${url.url}</textarea>`;
		htmlView += "</li>";
	})
	htmlView += "</ul>";
	return htmlView;
}

function printReadyToCopyUrl(urlsArray) {
	//prepire printable html elements
	let htmlView = `<h6>URL <a href="javascript:void(0)" class="float-right text-muted copy-btn" data-toggle="tooltip" title="Copied !"><i class="fa fa-copy"></i></a></h6>`
	htmlView += "<textarea class='form-control'>";
	//loop throw urls to print reguler results
	urlsArray.forEach(url => {
		htmlView += `${url.url}

`;
	})
	htmlView += "</textarea>";
	return htmlView;
}


//remove last slash in url
function removeLastSlash(url) {
	return url.replace(/(?:\/+(\?))/, '$1').replace(/\/+$/, '')
}


//register event listener for copy btn 
function copyBtnEventlistenr() {
	//copy btn 
	const copyBtns = document.querySelectorAll('.copy-btn');
	copyBtns.forEach(btn => {
		btn.addEventListener('click', function () {
			//move out to button then to its parent li then get last chaild of li wich is the text area the read its content
			const textToCopy = btn.parentElement.parentElement.lastElementChild.textContent;
			//invoke copy function
			copyToClipBoard(textToCopy);

			//hide all other tooltips
			$('.copy-btn').tooltip('disable');
			//show tooltip
			$(this).tooltip('enable');
			$(this).tooltip('show');
		})
	});

	//play tooltips
	// $(function () {
	// 	$('[data-toggle="tooltip"]').tooltip()
	// })
}