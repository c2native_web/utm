//catch html elements
const reslut = document.querySelector('#reslut');
const reslutTabBtn = document.querySelector('#results-btn');

const reslutsTocopy = document.querySelector('#resluts-tocopy');
const reslutsTocopyTabBtn = document.querySelector('#results-tocopy-btn');
export function controlResultsTab(){
	//reguler result tab
	reslutTabBtn.addEventListener('click', function(){
		reslutTabBtn.classList.add('active');
		reslut.classList.replace('d-none', 'd-block');

		reslutsTocopyTabBtn.classList.remove('active');
		reslutsTocopy.classList.replace('d-block', 'd-none');
	});


	//ready to copy result tab
	reslutsTocopyTabBtn.addEventListener('click', function () {
		reslutTabBtn.classList.remove('active');
		reslut.classList.replace('d-block', 'd-none');

		reslutsTocopyTabBtn.classList.add('active');
		reslutsTocopy.classList.replace('d-none', 'd-block');
	});
}