const mediaScheduleErrorElement = document.querySelector('#mediaScheduleError');
const objectiveErrorElement = document.querySelector('#objectiveError');
const languageErrorElement = document.querySelector('#languageError');
// validate from input
export function validateFormInput(formData) {
	
	//media schedul check
	if (!formData.get('media-schedule')) {
		mediaScheduleErrorElement.innerText = "Please select the source";
		scrollToView(mediaScheduleErrorElement.parentElement);
		return false;
	}

	//objective check
	if (!formData.get('objective')) {
		objectiveErrorElement.innerText = "Please select the medium";
		scrollToView(objectiveErrorElement.parentElement);
		return false;
	}


	// language check
	if (!formData.get('language')) {
		languageErrorElement.innerText = "Please select the language";
		scrollToView(languageErrorElement.parentElement);
		return false;
	}

}

// check for empty inpust
function isEmpty(input) {
	return input.trim() == "" ? true : false;
}

//scroll screen to view element
function scrollToView(element) {
	element.scrollIntoView();
}