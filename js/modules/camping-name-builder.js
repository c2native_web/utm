//catch html result area
const resultArea = document.querySelector("#result-area");
const resultText = document.querySelector("#reslut");
export function buildCampingName(fomrData) {
	const nameOptions = {
		mediaSchedule: fomrData.get('media-schedule'),
		objective: fomrData.get('objective'),
		market: fomrData.getAll('market'),
		audience: fomrData.get('audience'),
		language: fomrData.get('language'),
		creative: fomrData.get('creative'),
	}
	//show result area
	resultArea.classList.replace('d-none', 'd-flex');

	//genrate name
	const namesOutput = nameGenerator(nameOptions)
	//print name result
	resultText.innerHTML = printname(namesOutput);
	//scroll screen to view results
	resultArea.scrollIntoView();
}

function nameGenerator(nameOptions) {
	//set the media schedule
	let name = nameOptions.mediaSchedule;
	//set objective
	name += "_" + nameOptions.objective;
	//set market
	nameOptions.market ? name += "_" + nameOptions.market : '';
	//set audience
	nameOptions.audience ? name += "_" + nameOptions.audience : '';
	//set language
	nameOptions.language ? name += "_" + nameOptions.language : '';
	//set creative
	nameOptions.creative ? name += "_" + nameOptions.creative : '';

	return name;
}

function printname(name) {
	//prepire printable html elements

	let htmlView = `<h6>Result: </h6>`
	htmlView += `<textarea class='form-control'>${name}</textarea>`;
	return htmlView;
}