//catch html elements
const urlErrorElement = document.querySelector('#urlError');
const sourceErrorElement = document.querySelector('#sourceError');
const mediumErrorElement = document.querySelector('#mediumError');
const contentErrorElement = document.querySelector('#contentError');
// const languageErrorElement = document.querySelector('#languageError');
// const campaignErrorElement = document.querySelector('#campaignError');
// validate from input
export function validateFormInput(formData){
	//url empty check
	if (isEmpty(formData.get('url'))){
		urlErrorElement.innerText = "URL is required ";
		scrollToView(urlErrorElement.parentElement);
		return false;
	}

	//source check
	if (!formData.get('source')) {
		sourceErrorElement.innerText = "Please select the source";
		scrollToView(sourceErrorElement.parentElement);
		return false;
	}

	//medium check
	if (!formData.get('medium')) {
		mediumErrorElement.innerText = "Please select the medium";
		scrollToView(mediumErrorElement.parentElement);
		return false;
	}

	//check content
	if(!formData.getAll('content').length){
		contentErrorElement.innerText = "Please select one content";
		scrollToView(contentErrorElement.parentElement);
		return false;
	}

	//language check
	// if (!formData.get('language')) {
	// 	languageErrorElement.innerText = "Please select the language";
	// 	scrollToView(languageErrorElement.parentElement);
	// 	return false;
	// }

	//campaign check
	// if (isEmpty(formData.get('campaign'))) {
	// 	campaignErrorElement.innerText = "campaign text is required";
	// 	scrollToView(campaignErrorElement.parentElement);
	// 	return false;
	// }
}

// check for empty inpust
function isEmpty (input) {
	return input.trim() == "" ? true : false;
}

//scroll screen to view element
function scrollToView(element){
	element.scrollIntoView();
}