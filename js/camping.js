import { validateFormInput } from "./modules/camping-form-validation.js";
import { buildCampingName } from "./modules/camping-name-builder.js";
//catch html elements
const urlForm = document.querySelector('#url-builder-form');

//handle submit 
urlForm.addEventListener('submit', processUrlBuild);

//process url building
function processUrlBuild(event) {
	event.preventDefault();
	//catch form data
	const urlFormData = new FormData(this);

	//remove elements errors
	urlForm.querySelectorAll('small').forEach(element => {
		element.textContent = "";
	})

	//validate input
	let validationStatus = validateFormInput(urlFormData);

	//stop process genration when validation not passes
	if (validationStatus === false) return;

	//build url
	buildCampingName(urlFormData);
}
