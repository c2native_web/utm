<?php
//fetch stored data storage json file
$storage = json_decode(file_get_contents("./store/storage.json"), true);
//catch file load exceptions
if (!$storage) {
	// return error page 
	header('Location:500.html');
}
//fetch storage data
$sources = $storage['sources'];
$mediums = $storage['mediums'];
$contents = $storage['contents'];
$terms = $storage['terms'];
$languages = $storage['languages'];
$audiances = $storage['audiances'];

//add new content
if (isset($_POST['addContent'])) {
	$newContent['name'] = $_POST['lable'];
	$newContent['value'] =  rawurlencode($_POST['value']);
	//push new content to he contents array
	$contents[] = $newContent;
	//update storage array
	$storage['contents'] = $contents;
	//save storage array to json file
	$fp = fopen('./store/storage.json', 'w');
	fwrite($fp, json_encode($storage));
	fclose($fp);
	//rediruct user back
	header("Location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>UTM Generator</title>
	<!-- style files-->
	<link rel="stylesheet" href="./vendor/font-awesome/font-awesome.css" />
	<link rel="stylesheet" href="./vendor/bootstrap/bootstrap.css" />
	<link rel="stylesheet" href="./style/main.css" />

	<!-- scripts -->
	<script src="./vendor/jquery/jquery.min.js" defer></script>
	<script src="./vendor/popper/popper.min.js" defer></script>
	<script src="./vendor/bootstrap/bootstrap.min.js" defer></script>
	<script src="js/main.js" type="module" defer></script>
</head>

<body class="bg-light">
	<div class="bg-white">
		<nav class="navbar navbar-light container">
			<a class="navbar-brand m-0" href="#">
				<img src="./images/logo.png">
			</a>

			<div class="form-inline">
				<a href="./campaign.html" class="btn btn-secondary my-2 my-sm-0" type="submit">Campaign Generator</a>
			</div>
		</nav>
	</div>
	<header class="bg-white text-muted d-flex align-items-end">
		<div class="header-title container text-center font-weight-bolder">
			<p><b>UTM Generator</b></p>
		</div>
	</header>

	<main>
		<div class="d-flex container justify-content-center my-3">
			<div class="card card-body col-sm-10 col-lg-8 col-xl-6">
				<h5 class="text-secondary">Enter the website URL and campaign information</h5>
				<hr>
				<form id="url-builder-form">
					<!-- URL -->
					<div class="form-group">
						<label><b class="h6">Website URL<span class="text-danger"> *</span></b></label>
						<input type="url" name="url" class="form-control" placeholder="The full website URL (e.g. https://www.example.com)">
						<small id="urlError" class="form-text text-danger"></small>
					</div>
					<!-- #ulr -->

					<!-- Source -->
					<div class="form-group">
						<label><b class="h6">Source<span class="text-danger"> *</span></b></label>
						<select name="source" class="form-control">
							<option selected disabled>Select Campaign Source</option>
							<?php foreach ($sources as $src) { ?>
								<option value="<?php echo $src['value'] ?>">
									<?php echo $src['name'] ?>
								</option>
							<?php } ?>
						</select>
						<small id="sourceError" class="form-text text-danger"></small>
					</div>
					<!-- #Source -->

					<!-- Medium -->
					<div class="form-group">
						<label><b class="h6">Medium<span class="text-danger"> *</span></b></label>
						<select name="medium" class="form-control">
							<option selected disabled>Select Campaign Medium</option>
							<?php foreach ($mediums as $m) { ?>
								<option value="<?php echo $m['value'] ?>">
									<?php echo $m['name'] ?>
								</option>
							<?php } ?>
						</select>
						<small id="mediumError" class="form-text text-danger"></small>
					</div>
					<!-- #medium -->

					<!--Content -->
					<div class="form-group mt-4">
						<div class="d-flex justify-content-between">
							<label><b class="h6">Content<span class="text-danger"> *</span></b></label>
							<label><a href="javascript:void(0)" class="btn btn-outline-secondary" data-toggle="modal" data-target="#addContent">Add New</a></label>
						</div>
						<div class=" d-flex flex-wrap">
							<?php foreach ($contents as $key => $content) { ?>
								<div class="form-check mr-3 mb-2">
									<input name="content" class="form-check-input" type="checkbox" value="<?php echo $content['value']; ?>" id="<?php echo $content['value'] . $key; ?>">
									<label class="form-check-label" for="<?php echo $content['value'] . $key; ?>">
										<?php echo $content['name']; ?>
									</label>
								</div>
							<?php } ?>

						</div>
						<small id="contentError" class="form-text text-danger"></small>
					</div>
					<!-- #content-->

					<!--Term -->
					<div class="form-group">
						<label>Term</label>
						<div class=" d-flex flex-wrap">

							<?php foreach ($terms as $key => $term) { ?>
								<div class="form-check mr-3 mb-2">
									<input name="term" class="form-check-input" type="checkbox" value="<?php echo $term['value']; ?>" id="<?php echo $term['value'] . $key; ?>">
									<label class="form-check-label" for="<?php echo $term['value'] . $key; ?>">
										<?php echo $term['name']; ?>
									</label>
								</div>
							<?php } ?>

						</div>
					</div>
					<!-- #term-->

					<!-- Language -->
					<div class="form-group">
						<label><b class="h6">Language</b></label>
						<select name="language" class="form-control">
							<option selected disabled>Select Campaign Language</option>
							<?php foreach ($languages as $language) { ?>
								<option value="<?php echo $language['value'] ?>">
									<?php echo $language['name'] ?>
								</option>
							<?php } ?>
						</select>
						<small id="languageError" class="form-text text-danger"></small>
					</div>
					<!-- #language -->

					<!-- Campaign -->
					<div class="form-row">
						<div class="form-group <?php if (count($audiances)) echo 'col-sm-6';
																		else echo 'col-sm-12'; ?> ">
							<label><b class="h6">Campaign</b></label>
							<input type="text" name="campaign" class="form-control" placeholder="Campaign">
							<small id="campaignError" class="form-text text-danger"></small>
						</div>
						<?php if (count($audiances)) { ?>
							<div class="form-group col-sm-6">
								<label><b class="h6">Audiances</b></label>
								<select name="audiance" class="form-control">
									<option selected disabled>Select Campaign Language</option>
									<?php foreach ($audiances as $audiance) { ?>
										<option value="<?php echo $audiance['value'] ?>">
											<?php echo $audiance['name'] ?>
										</option>
									<?php } ?>
								</select>
								<small id="audianceError" class="form-text text-danger"></small>
							</div>
						<?php } ?>
					</div>
					<!-- #Campaign -->
					<button type="submit" class="btn btn-secondary">Genrate URL</button>
				</form>
			</div>
		</div>

		<!-- result area-->
		<div class="container my-3 justify-content-center d-none" id="result-area">
			<div class="col-sm-10 card card-body col-lg-8 col-xl-6">
				<h5 class="text-secondary">Genrated Url</h5>
				<hr>
				<ul class="nav nav-pills">
					<li class="nav-item">
						<a class="nav-link active" href="javascript:void(0)" id="results-btn">Results</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="javascript:void(0)" id="results-tocopy-btn">Ready TO Copy</a>
					</li>
				</ul>

				<p id="reslut" class="d-block">
					<!-- resluts html generated from js/modules/url-builder#printUrl():l25 -->
				</p>
				<p id="resluts-tocopy" class="d-none">
					<!-- resluts html generated from js/modules/url-builder#printUrl():l25 -->
				</p>
			</div>
		</div>
	</main>


	<!-- add content modal -->
	<div class="modal fade" id="addContent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-body">
				<h4 class="text-center m-4">
					Add New Content
				</h4>
				<form action="" method="post">
					<div class="form-group">
						<label><b class="h6">Label<span class="text-danger"> *</span></b></label>
						<input type="text" name="lable" class="form-control" placeholder="The lable to display in the form" required>
					</div>
					<div class="form-group">
						<label><b class="h6">Value<span class="text-danger"> *</span></b></label>
						<input type="text" name="value" class="form-control" placeholder="Value to add to URL" required>
					</div>
					<div class="modal-footer d-flex justify-content-around">
						<button type="submit" name="addContent" class="btn btn-secondary">Add Content</button>
						<button type="button" class="btn" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

</html>